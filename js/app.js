var MyApp = angular
    .module('music', [
        'ui.router',
        'ngCookies'
    ]);

MyApp.run(function ($rootScope, $state, Auth) {
    $rootScope.$on("$stateChangeStart", function (event, toState, toParams, fromState, fromParams) {
        if (toState.authentication && !Auth.isLoggedIn()) {
            $state.transitionTo("login");
            event.preventDefault();
        } else if (!toState.authentication && Auth.isLoggedIn()) {
            $state.transitionTo("tracks");
            event.preventDefault();
        }
    });
});


MyApp.config(["$stateProvider", "$urlRouterProvider", "$sceDelegateProvider", function ($stateProvider, $urlRouterProvider, $sceDelegateProvider) {

    $urlRouterProvider.otherwise("/login");

    $sceDelegateProvider.resourceUrlWhitelist([
        'self',
        '*'
    ]);

    $stateProvider
        .state('login', {
            url: '/login',
            controller: 'LoginController',
            controllerAs: 'login',
            templateUrl: 'login.html'
        })
        .state('tracks', {
            url: '/tracks',
            controller: 'TrackController',
            controllerAs: 'trackCtrl',
            templateUrl: 'tracks.html',
            authentication: true
        })
        .state('playlists', {
            url: '/playlists',
            controller: 'PlaylistController',
            controllerAs: 'playlistCtrl',
            templateUrl: 'playlists.html',
            authentication: true
        })
        .state('playlists.tracks', {
            url: '/:id',
            controller: 'PlaylistTrackController',
            controllerAs: 'playlistTrackCtrl',
            templateUrl: 'playlist-tracks.html',
            authentication: true
        });
}]);