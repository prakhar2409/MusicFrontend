(function () {
    'use strict';

    MyApp.factory("sessionStorageService", sessionStorageService);

    sessionStorageService.$inject = ['$cookies'];

    function sessionStorageService($cookies) {
        function set(key, data) {
            $cookies.putObject(key, data || null);
        }

        function get(key) {
            var data = $cookies.getObject(key);
            return data || null;
        }

        function remove(key) {
            $cookies.remove(key);
        }

        return {
            set: set,
            get: get,
            remove: remove
        }
    }
})();