(function () {
    'use strict';

    MyApp.controller('PlaylistTrackController', PlaylistTrackController);

    PlaylistTrackController.$inject = ['$stateParams', 'PlaylistService', 'TrackService'];

    function PlaylistTrackController($stateParams, PlaylistService, TrackService) {

        var playlistTrackCtrl = this;
        playlistTrackCtrl.playlistId = $stateParams.id;

        playlistTrackCtrl.getTracks = function () {
            PlaylistService.getTracks(playlistTrackCtrl.playlistId, playlistTrackCtrl);
        };

        playlistTrackCtrl.getTracks();

        playlistTrackCtrl.removeTrackFromPlaylist = function (trackObj) {
            TrackService.toggleTrackFromPlaylist(playlistTrackCtrl.playlistId, trackObj.id, function (response) {
                if (response.data.status) {
                    playlistTrackCtrl.tracks.splice(playlistTrackCtrl.tracks.indexOf(trackObj), 1);
                }
            });
        };
    }
})();