(function () {
    'use strict';

    MyApp.controller('LoginController', LoginController);

    LoginController.$inject = ['$state', 'Auth'];

    function LoginController($state, Auth) {
        var login = this;

        login.login = function () {
            login.loggingIn = true;
            var user = {
                username: login.username,
                password: login.password
            };
            Auth.login(user, function (data) {
                if (data) {
                    login.loggingIn = false;
                    login.username = "";
                    login.password = "";
                    login.errorMessage = data;
                } else {
                    $state.go('tracks');
                }
            });
        };
    }
})();
