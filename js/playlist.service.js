(function () {
    'use strict';

    MyApp.factory('PlaylistService',playlistService);

    playlistService.$inject = ['$http', 'API', 'Auth'];

    function playlistService($http, API, Auth) {
        var user = Auth.getAuthDetails();

        function getPlaylists(scope) {
            $http.get(API.BASE + "/playlists/" + user.id).then(function (response) {
                scope.playlists = response.data;
            }, function () {
                console.log("Something went wrong");
            });
        }

        function getTracks(playlistId, scope) {
            $http.get(API.BASE + "/tracks/" + user.id + "/playlist/" + playlistId).then(function (response) {
                scope.tracks = response.data;
            }, function () {
                console.log("Something went wrong");
            });
        }

        function createPlaylist(playlistName, callback) {
            $http.post(API.BASE + "/playlist/create/" + user.id, {
                name: playlistName
            }).then(function (response) {
                if (response.data.status == true) {
                    callback(response.data.data);
                } else {
                    callback(null);
                }
            }, function () {
                console.log("Something went wrong");
            });
        }

        return {
            getPlaylists: getPlaylists,
            getTracks: getTracks,
            createPlaylist: createPlaylist
        }
    }
})();